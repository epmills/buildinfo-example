package org.example;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static generated.BuildInfo.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestReporter;

public class BuildInfoTest {
    @Test
    public void testBuildInfo(TestReporter reporter) {

        final var rxArtifact = "[A-Za-z0-9-]+";
        final var rxVersion = "[0-9]+\\.[0-9]+\\.[0-9]+(.+)?";
        final var rxRevFull = "[0-9a-f]{40}([\\+]?)";
        final var rxRevShort = "[0-9a-f]{9}([\\+]?)";
        final var rxDate = ".{4}\\-.{2}\\-.{2}";
        final var rxTime = ".{2}:.{2}.{2}";
        final var rxDateTime = String.format("%sT%s.*", rxDate, rxTime);
        final var rxBuildInfo = String.format("%s %s \\(%s %s\\)", rxArtifact, rxVersion, rxRevShort, rxDate);

        var actualArtifact = ARTIFACT;
        var actualVersion = VERSION;
        var actualRevFull = REV_FULL;
        var actualRevShort = REV_SHORT;
        var actualBuildDate = BUILD_DATE;
        var actualBuildDateTime = BUILD_DATETIME;
        var actualBuildInfo = BUILD_INFO;

        reporter.publishEntry("Artifact", actualArtifact);
        reporter.publishEntry("Version", actualVersion);
        reporter.publishEntry("RevFull", actualRevFull);
        reporter.publishEntry("RevShort", actualRevShort);
        reporter.publishEntry("BuildDate", actualBuildDate);
        reporter.publishEntry("BuildDateTime", actualBuildDateTime);
        reporter.publishEntry("BuildInfo", actualBuildInfo);

        assertEquals("buildinfo-example", actualArtifact);
        assertTrue(actualVersion.matches(rxVersion));
        assertTrue(actualRevFull.matches(rxRevFull));
        assertTrue(actualRevShort.matches(rxRevShort));
        assertTrue(actualBuildDate.matches(rxDate));
        assertTrue(actualBuildDateTime.matches(rxDateTime));
        assertTrue(actualBuildInfo.matches(rxBuildInfo));
    }
}
