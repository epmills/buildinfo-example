package org.example;

import static generated.BuildInfo.ARTIFACT;
import static generated.BuildInfo.BUILD_DATE;
import static generated.BuildInfo.BUILD_DATETIME;
import static generated.BuildInfo.BUILD_INFO;
import static generated.BuildInfo.BUILD_DIRTY;
import static generated.BuildInfo.REV_FULL;
import static generated.BuildInfo.REV_SHORT;
import static generated.BuildInfo.VERSION;

/**
 * Build information example.
 *
 * Demonstrates gathering build information at compile time, generating a Java
 * class containing that information and emitting it to standard output without
 * requiring a change to this file.
 */
public class App {

    /**
     * Entrypoint for this application.
     * 
     * @param ignored command line arguments (ignored)
     */
    public static void main(String[] ignored) {
        System.out.printf("BUILD_INFO:     %s\n", BUILD_INFO);
        System.out.printf("ARTIFACT:       %s\n", ARTIFACT);
        System.out.printf("VERSION:        %s\n", VERSION);
        System.out.printf("REV_FULL:       %s\n", REV_FULL);
        System.out.printf("REV_SHORT:      %s\n", REV_SHORT);
        System.out.printf("BUILD_DIRTY     %b\n", BUILD_DIRTY);
        System.out.printf("BUILD_DATETIME: %s\n", BUILD_DATETIME);
        System.out.printf("BUILD_DATE:     %s\n", BUILD_DATE);
    }
}
