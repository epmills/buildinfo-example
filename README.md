# Build Info Example

This is an example Maven project demonstrating how to generate build information
at compile time without any external plugins.

## Motivation

The solution originates from frustration generating build information -
including artifact name, version, latest commit ID and build date - with Maven
versions as old as 3.3.9.

## Solution

This solution involves solely additions to an existing `pom.xml`, adding and
configuring commonly-available plugins `maven-antrun-plugin` and
`build-helper-maven-plugin`.

## Technique

Copy the lines in the `pom.xml` file between `<!-- BEGIN COPY -->` and `<!-- END
COPY -->` and paste them into a project's `pom.xml` file within the `<plugins>`
section.

Then perform a `mvn clean package` command.

Now, the generated `BuildInfo.java` class' properties are available in your
project:

```java
import static generated.BuildInfo.BUILD_INFO;
...
System.out.println(BUILD_INFO);
```

## Notes
By default, the generated `BuildInfo.java` file will *not* contain JavaDoc
comments.  To include them in your project, replace the contents of the `CDATA`
section of the `pom.xml` with the contents of
`src/template/java/org/example/BuildInfo.java.template`.